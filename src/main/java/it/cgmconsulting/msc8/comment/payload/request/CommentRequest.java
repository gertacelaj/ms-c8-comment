package it.cgmconsulting.msc8.comment.payload.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;

@Getter
public class CommentRequest {
	
	@NotBlank @Size(max=255, min=10)
	private String content;
	
	@Min(1)
	private long postId;

}
