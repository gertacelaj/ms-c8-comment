package it.cgmconsulting.msc8.comment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.cgmconsulting.msc8.comment.entity.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long>{

}
