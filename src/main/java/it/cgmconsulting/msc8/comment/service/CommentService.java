package it.cgmconsulting.msc8.comment.service;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import it.cgmconsulting.msc8.comment.entity.Comment;
import it.cgmconsulting.msc8.comment.repository.CommentRepository;
import it.cgmconsulting.msc8.comment.utils.Constants;

@Service
public class CommentService {
	
	@Autowired CommentRepository commentRepository;
	@Autowired RestTemplate restTemplate;		
	
	public Comment save(Comment c) {
		return commentRepository.save(c);	
	}
	
	public ResponseEntity<?> verifyPost(long id){
		//String url = "http://localhost:8089/post/exists/"+id;
		String url = Constants.MS_POST_URL+"/exists/"+id;
			
		
		try {
			URI uri = new URI(url);
			return new ResponseEntity<Object>(restTemplate.getForObject(uri, Object.class), HttpStatus.OK);
		} catch (URISyntaxException e) {
			return new ResponseEntity<String>("URI SYINTAX EXCEPTION", HttpStatus.BAD_REQUEST);
		} catch (RestClientException ex) {
			return new ResponseEntity<String>("Rest Client Exception", HttpStatus.NOT_FOUND);
		}
	}

}
