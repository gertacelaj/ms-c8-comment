package it.cgmconsulting.msc8.comment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.cgmconsulting.msc8.comment.entity.Comment;
import it.cgmconsulting.msc8.comment.payload.request.CommentRequest;
import it.cgmconsulting.msc8.comment.service.CommentService;

@RestController
public class CommentController {
	
	@Autowired CommentService commentService;
	
	@PostMapping("/")
	public ResponseEntity<?> create(@RequestBody CommentRequest commentRequest){
		
		ResponseEntity<?> response =  commentService.verifyPost(commentRequest.getPostId());
		if(response.getStatusCode() == HttpStatus.OK) {
			commentService.save(new Comment(commentRequest.getContent(), commentRequest.getPostId()));
			return new ResponseEntity<String>("New comment has been created", HttpStatus.OK);
		}else 
			return new ResponseEntity<String>("Post not found", response.getStatusCode());
	}
		
	

}
